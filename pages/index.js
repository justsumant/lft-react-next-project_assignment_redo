import { Fragment } from "react/cjs/react.production.min";
import LoginComponent from "../components/login";

/**
 * Enntry page (home), by defailt user will be directed to this login page
 */
export default function Home() {
  return (
    <Fragment>
      <LoginComponent />
    </Fragment>
  );
}
