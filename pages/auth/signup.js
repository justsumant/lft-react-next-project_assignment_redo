import { Fragment } from "react";
import SignUpComponent from "../../components/signup";

/**
 * Normal Signup page, it calls signup(form) component.
 */
function SignupPage() {
  return (
    <Fragment>
      <SignUpComponent />
    </Fragment>
  );
}

export default SignupPage;
