import { useCallback, useEffect, useState } from "react";
import { Fragment } from "react/cjs/react.production.min";
import ContactFormComponent from "../../components/contactForm";
import ContactsComponent from "../../components/contacts";
import NavBar from "../../components/navbar";
import { useRouter } from "next/router";

/**
 * Contacts page, It contains 3 components (nanvbar, contact list and contact add/update form) and allows communication between them
 *
 * */
function ContactsPage() {
  const router = useRouter();
  // global constants for intercommunication between 3 components
  const [contacts, setContacts] = useState([]);
  const [isUpdateState, setUpdateState] = useState(false);
  const [updatableContact, setUpdatableContact] = useState();

  const baseURL = "http://localhost:3001/";

  /**
   * fetches contacts sending a http request to backend
   */
  const fetchContacts = useCallback(async () => {
    try {
      //setting headers getting from local storage
      const headers = {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage?.getItem("token"),
      };
      await fetch(baseURL + "getContacts", { headers })
        .then((response) => response.json())
        .then((data) => {
          if (data.statusCode === 500 || data.statusCode === 401) {
            router.push("/");
          } else {
            setContacts(data);
          }
        });
    } catch (error) {
      console.log(error);
      router.push("/");
    }
  }, []);

  // useeffect to fetch contacts when the page loads for the first time.
  useEffect(() => {
    fetchContacts();
  }, [fetchContacts]);

  return (
    <Fragment>
      <NavBar />
      <div className="container mt-4">
        <div className="row">
          <div className="col-8">
            <ContactsComponent
              contacts={contacts}
              onFetchContacts={fetchContacts}
              setUpdateState={setUpdateState}
              setUpdatableContact={setUpdatableContact}
            />
          </div>
          <div className="col-4">
            <ContactFormComponent
              onFetchContacts={fetchContacts}
              isUpdateState={isUpdateState}
              setUpdateState={setUpdateState}
              updatableContact={updatableContact}
            />
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default ContactsPage;
