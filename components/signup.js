import Link from "next/link";
import { Fragment, useState } from "react";
import { useRouter } from "next/router";

/**
 * brings signup form in signup page, performs signup functionlaity
 */
function SignUpComponent() {
  const router = useRouter();
  // form properties
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  // form validation properties
  const [emailIsValid, setEmailValidation] = useState(true);
  const [nameIsValid, setNameValidation] = useState(true);
  const [passwordIsValid, setPasswordValidation] = useState(true);
  const [cPasswordIsValid, setCPasswordValidation] = useState(true);

  // form field handler
  const emailHandler = (event) => {
    let email = event.target.value;
    setEmail(email);
    if (
      email &&
      email
        .toLowerCase()
        .match(
          /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        )
    ) {
      setEmailValidation(true);
    } else {
      setEmailValidation(false);
    }
  };

  // form field handler
  const passwordHandler = (event) => {
    const password = event.target.value;
    setPassword(password);
    if (
      password &&
      password.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{6,}$/)
    ) {
      setPasswordValidation(true);
    } else {
      setPasswordValidation(false);
    }
  };

  // form field handler
  const nameHandler = (event) => {
    const name = event.target.value;
    setName(name);
    if (name && name.length > 3) {
      setNameValidation(true);
    } else {
      setNameValidation(false);
    }
  };

  // form field handler
  const confirmPasswordHandler = (event) => {
    const cPassword = event.target.value;
    setConfirmPassword(cPassword);
    if (cPassword && cPassword === password) {
      setCPasswordValidation(true);
    } else {
      setCPasswordValidation(false);
    }
  };

  // clears the form and sets to initial
  const clearForm = () => {
    setEmail("");
    setPassword("");
    setConfirmPassword("");
    setName("");
  };

  // submits signup form
  const submitSignupForm = (event) => {
    event.preventDefault();
    if (password == confirmPassword) {
      const user = {
        email: email,
        password: password,
        name: name,
      };
      // sets header
      const requestOptions = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(user),
      };

      // send signup http request
      fetch(
        // "https://api.sumantgupta.com.np/auth/signup",
        "http://localhost:3001/auth/signup",
        requestOptions
      )
        .then((response) => response.json())
        .then((data) => {
          router.push("/");
          console.log(data);
        });

      clearForm();
    } else {
      alert("passwords dont match!!!");
    }
  };

  return (
    <Fragment>
      <div className="container mt-4">
        <div className="row">
          <div className="col-12">
            <div className="card mt-4 mx-auto signup-card">
              <div className="text-center mb-4">
                <img
                  src="https://angular.sumantgupta.com.np/assets/images/logo.png"
                  alt="logo"
                  height="80px"
                />
                <h4>Welcome</h4>
                <p>Sign up by entering your informations below.</p>
              </div>
              <div className="card-body">
                <form onSubmit={submitSignupForm}>
                  <div className="mb-3">
                    <label htmlFor="name" className="form-label">
                      Name
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      value={name}
                      onChange={nameHandler}
                      placeholder="john banner"
                    />
                    {nameIsValid === false ? (
                      <small className="inputError" style={{ color: "red" }}>
                        Please enter a valid name.
                      </small>
                    ) : (
                      <></>
                    )}
                  </div>
                  <div className="mb-3">
                    <label htmlFor="username" className="form-label">
                      Email
                    </label>
                    <input
                      type="email"
                      className="form-control"
                      value={email}
                      onChange={emailHandler}
                      placeholder="name@example.com"
                    />
                    {emailIsValid === false ? (
                      <small className="inputError" style={{ color: "red" }}>
                        Please enter a valid email ID.
                      </small>
                    ) : (
                      <></>
                    )}
                  </div>
                  <div className="mb-3">
                    <label htmlFor="password" className="form-label">
                      Password (min length 6, small, capital, digit)
                    </label>
                    <input
                      type="password"
                      className="form-control"
                      value={password}
                      onChange={passwordHandler}
                      placeholder="**********"
                    />
                    {passwordIsValid === false ? (
                      <small className="inputError" style={{ color: "red" }}>
                        Please enter a valid Password.
                      </small>
                    ) : (
                      <></>
                    )}
                  </div>
                  <div className="mb-3">
                    <label htmlFor="confirmPassword" className="form-label">
                      Confirm Password
                    </label>
                    <input
                      type="password"
                      className="form-control"
                      value={confirmPassword}
                      onChange={confirmPasswordHandler}
                      placeholder="**********"
                    />
                    {cPasswordIsValid === false ? (
                      <small className="inputError" style={{ color: "red" }}>
                        Passwords dont match.
                      </small>
                    ) : (
                      <></>
                    )}
                  </div>
                  <div className="mb-3">
                    <button
                      type="button"
                      className="btn btn-outline-dark mx-2"
                      onClick={clearForm}
                    >
                      Cancel
                    </button>
                    <button
                      type="submit"
                      className="btn btn-secondary mx-2"
                      disabled={
                        !(
                          emailIsValid &&
                          passwordIsValid &&
                          cPasswordIsValid &&
                          nameIsValid &&
                          email &&
                          password &&
                          confirmPassword &&
                          name
                        )
                      }
                    >
                      Sign Up
                    </button>
                  </div>
                </form>
                <div className="d-flex flex-row-reverse">
                  <div>
                    <Link href="/">Back to Login?</Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default SignUpComponent;
