import { useEffect, useState, useCallback } from "react";

const { Fragment } = require("react/cjs/react.production.min");

/**
 * this component is responsible to display contacts received from parent page as
 * props and return methods like delete and update to parent
 */
function ContactsComponent(props) {
  const contacts = props.contacts;

  const baseURL = "http://localhost:3001/";
  // const baseURL = 'https://api.sumantgupta.com.np/';

  /**
   * It will delete a contact from contact list (action triggered when delete button clicked on particular contact)
   */
  const deleteContactHandler = (id) => {
    // setting headers
    const headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + localStorage?.getItem("token"),
    };
    const requestOptions = {
      method: "DELETE",
      headers: headers,
    };
    // send delete request to backend
    fetch(baseURL + "deleteContact" + id, requestOptions)
      .then((response) => response.json())
      .then((data) => {
        props.onFetchContacts();
      });
  };

  /**
   * toggles selected contact between fav and non fav, fetches new list on update completed.
   */
  const toggleFavorite = (contact) => {
    // setting headers
    const headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + localStorage?.getItem("token"),
    };
    contact.isFavorite = contact.isFavorite === 0 ? 1 : 0;
    const requestOptions = {
      method: "POST",
      headers: headers,
      body: JSON.stringify(contact),
    };

    fetch(baseURL + "updateContact", requestOptions)
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        props.onFetchContacts();
      });
  };

  /**
   * sends data to parent to update a contact (updatable contact is passed to parent then to the form)
   */
  const updateContact = (contact) => {
    props.setUpdatableContact(contact);
    props.setUpdateState(true);
  };
  return (
    <Fragment>
      <h4>All Contacts</h4>
      <div className="fixed-box">
        {contacts.length === 0 ? (
          <p>No contact found, try to add some first.</p>
        ) : (
          contacts.map((contact) => (
            <div className="card my-3" key={contact.id}>
              <div className="card-header">
                <div className="d-flex justify-content-between">
                  <div>
                    <b>{contact.name}</b>, {contact.address}
                  </div>
                  <div>
                    {contact.isFavorite == 0 || false ? (
                      <button
                        className="starButton"
                        onClick={() => toggleFavorite(contact)}
                      >
                        <i className="far fa-star"></i>
                      </button>
                    ) : (
                      <button
                        className="starButton"
                        onClick={() => toggleFavorite(contact)}
                      >
                        <i className="fas fa-star"></i>
                      </button>
                    )}
                  </div>
                </div>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-10">
                    <div className="d-flex justify-content-between">
                      {contact.contactNumber != undefined ? (
                        <h6>
                          <a href={"tel:" + contact.contactNumber}>
                            <i className="fas fa-mobile-alt"></i>{" "}
                            {contact.contactNumber}
                          </a>
                        </h6>
                      ) : (
                        <h6></h6>
                      )}
                      {contact.homeNumber != undefined ? (
                        <h6>
                          <a href={"tel:" + contact.homeNumber}>
                            <i className="fas fa-home"></i> {contact.homeNumber}
                          </a>
                        </h6>
                      ) : (
                        <h6></h6>
                      )}
                      {contact.officeNumber != undefined ? (
                        <h6>
                          <a href={"tel:" + contact.officeNumber}>
                            <i className="fas fa-building"></i>{" "}
                            {contact.officeNumber}
                          </a>
                        </h6>
                      ) : (
                        <h6></h6>
                      )}
                    </div>
                    <div>
                      <h6>
                        {" "}
                        <a href={"mailto:" + contact.email}>
                          <i className="fas fa-envelope"></i> {contact.email}
                        </a>
                      </h6>
                    </div>
                  </div>
                  <div className="col-2">
                    <button
                      type="button"
                      className="btn btn-danger btn-sm m-1"
                      onClick={() => deleteContactHandler(contact.id)}
                    >
                      <i className="fas fa-trash"></i>
                    </button>
                    <button
                      type="button"
                      className="btn btn-secondary btn-sm m-1"
                      onClick={() => updateContact(contact)}
                    >
                      <i className="fas fa-edit"></i>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          ))
        )}
      </div>
    </Fragment>
  );
}

export default ContactsComponent;
