import { useEffect, useState } from "react";
const { Fragment } = require("react/cjs/react.production.min");

/**
 * renders a contact from from where user can add or update a contact, updation time it will receive old contact as props from parent
 */
function ContactFormComponent(props) {
  // form elements
  const [nameInput, setName] = useState("");
  const [addressInput, setAddress] = useState("");
  const [contactInput, setContact] = useState("");
  const [officeContactInput, setOfficeContact] = useState("");
  const [homeContactInput, setHomeContact] = useState("");
  const [isFavoriteInput, setIsFav] = useState(false);
  const [emailInput, setEmail] = useState("");
  const [id, setId] = useState("");
  // form validation variables
  const [emailIsValid, setEmailValidation] = useState(true);
  const [nameIsValid, setNameValidation] = useState(true);
  const [addressIsValid, setAddressValidation] = useState(true);
  const [officeContactIsValid, setOfficeContactValidation] = useState(true);
  const [homeContactIsValid, setHomeContactValidation] = useState(true);
  const [contactIsValid, setContactValidation] = useState(true);

  let isUpdateState = props.isUpdateState;
  let updatableContact = props.updatableContact;
  // form variable handler
  const nameHandler = (event) => {
    const name = event.target.value;
    setName(name);
    if (name && name.length > 3) {
      setNameValidation(true);
    } else {
      setNameValidation(false);
    }
  };
  // form variable handler
  const contactHandler = (event) => {
    const contact = event.target.value;
    setContact(contact);
    if (
      contact &&
      contact.match(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/)
    ) {
      setContactValidation(true);
    } else {
      setContactValidation(false);
    }
  };
  // form variable handler
  const addressHandler = (event) => {
    const address = event.target.value;
    setAddress(address);
    if (address && address.length > 3) {
      setAddressValidation(true);
    } else {
      setAddressValidation(false);
    }
  };
  // form variable handler
  const officeContactHandler = (event) => {
    const officeContact = event.target.value;
    setOfficeContact(officeContact);
    if (
      !officeContact ||
      officeContact.match(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/)
    ) {
      setOfficeContactValidation(true);
    } else {
      setOfficeContactValidation(false);
    }
  };
  // form variable handler
  const homeContactHandler = (event) => {
    const homeContact = event.target.value;
    setHomeContact(homeContact);
    if (
      !homeContact ||
      homeContact.match(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/)
    ) {
      setHomeContactValidation(true);
    } else {
      setHomeContactValidation(false);
    }
  };
  // form variable handler
  const emailHandler = (event) => {
    const email = event.target.value;
    setEmail(email);
    if (
      email &&
      email
        .toLowerCase()
        .match(
          /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        )
    ) {
      setEmailValidation(true);
    } else {
      setEmailValidation(false);
    }
  };
  // form variable handler
  const isFavoriteHandler = (event) => {
    console.log(event.target.checked);
    setIsFav(event.target.checked);
  };

  /**
   * clears the form and sets to initial
   */
  const clearForm = () => {
    setEmail("");
    setName("");
    setContact("");
    setIsFav(false);
    setHomeContact("");
    setOfficeContact("");
    setAddress("");
    setId("");
    props.setUpdateState(false);
    console.log(isFavoriteInput);
  };

  /**
   * to run for only once when form is loaded,
   */
  useEffect(() => {
    // if the data coming from parent for updation, set the form with incoming data else do nothing
    if (isUpdateState) {
      setEmail(updatableContact.email);
      setName(updatableContact.name);
      setContact(updatableContact.contactNumber);
      setIsFav(updatableContact.isFavorite ? true : false);
      setHomeContact(updatableContact.homeNumber || "");
      setOfficeContact(updatableContact.officeNumber || "");
      setAddress(updatableContact.address || "");
      setId(updatableContact.id);
    }
  }, [updatableContact]);

  /**
   * on submission of form, old contact will be updated while new one will be added. the differentiation will be done on the basis of updatedState variable.
   */
  const addUpdateContact = (event) => {
    event.preventDefault();
    const contactData = {
      name: nameInput,
      contactNumber: contactInput !== "" ? parseInt(contactInput) : undefined,
      email: emailInput,
      homeNumber:
        homeContactInput !== "" ? parseInt(homeContactInput) : undefined,
      officeNumber:
        officeContactInput !== "" ? parseInt(officeContactInput) : undefined,
      address: addressInput,
      isFavorite: isFavoriteInput === true ? 1 : 0,
      id: id,
    };

    //setting header
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token"),
      },
      body: JSON.stringify(contactData),
    };

    if (isUpdateState) {
      // update the contact and reload the list sending function call request to parent
      fetch(
        // "https://api.sumantgupta.com.np/updateContact",
        "http://localhost:3001/updateContact",
        requestOptions
      )
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
          alert("contact updated successfully");
          props.onFetchContacts();
          clearForm();
        })
        .catch((err) => {
          alert("something went wrong... ", err);
        });
    } else {
      // add new contact and reload the list sending function call request to parent
      fetch(
        // "https://api.sumantgupta.com.np/addNewContact",
        "http://localhost:3001/addNewContact",
        requestOptions
      )
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
          alert("contact added successfully");
          props.onFetchContacts();
          clearForm();
        })
        .catch((err) => {
          alert("something went wrong... ", err);
        });
    }
  };

  return (
    <Fragment>
      <div className="card">
        <div className="card-header">Add New Contact</div>
        <div className="card-body">
          <form>
            <input type="hidden" id="id" value={id} />
            <div className="mb-3">
              <label className="form-label">Full Name</label>
              <input
                type="text"
                className="form-control"
                value={nameInput}
                onChange={nameHandler}
              />
              {nameIsValid === false ? (
                <small className="inputError" style={{ color: "red" }}>
                  Please enter a valid name.
                </small>
              ) : (
                <></>
              )}
            </div>
            <div className="mb-3">
              <label className="form-label">Email address</label>
              <input
                type="email"
                className="form-control"
                value={emailInput}
                onChange={emailHandler}
              />
              {emailIsValid === false ? (
                <small className="inputError" style={{ color: "red" }}>
                  Please enter a valid email ID.
                </small>
              ) : (
                <></>
              )}
            </div>
            <div className="mb-3">
              <label className="form-label">Contact Number</label>
              <input
                type="number"
                className="form-control"
                value={contactInput}
                onChange={contactHandler}
              />
              {contactIsValid === false ? (
                <small className="inputError" style={{ color: "red" }}>
                  Please provide a valid contact number.
                </small>
              ) : (
                <></>
              )}
            </div>
            <div className="mb-3">
              <label className="form-label">Home Contact Number</label>
              <input
                type="number"
                className="form-control"
                value={homeContactInput}
                onChange={homeContactHandler}
              />
              {homeContactIsValid === false ? (
                <small className="inputError" style={{ color: "red" }}>
                  Please provide a valid contact number.
                </small>
              ) : (
                <></>
              )}
            </div>
            <div className="mb-3">
              <label className="form-label">Office Contact Number</label>
              <input
                type="number"
                className="form-control"
                value={officeContactInput}
                onChange={officeContactHandler}
              />
              {officeContactIsValid === false ? (
                <small className="inputError" style={{ color: "red" }}>
                  Please provide a valid contact number.
                </small>
              ) : (
                <></>
              )}
            </div>
            <div className="mb-3">
              <label className="form-label">Address</label>
              <input
                type="address"
                className="form-control"
                value={addressInput}
                onChange={addressHandler}
              />
              {addressIsValid === false ? (
                <small className="inputError" style={{ color: "red" }}>
                  Please provide valid address.
                </small>
              ) : (
                <></>
              )}
            </div>
            <div className="mb-3 form-check">
              <input
                type="checkbox"
                className="form-check-input"
                checked={isFavoriteInput}
                onChange={isFavoriteHandler}
              />
              <label className="form-check-label">Make Favourite</label>
            </div>
            <div className="mb-3">
              <button
                className="btn btn-secondary mx-2"
                onClick={addUpdateContact}
                disabled={
                  !(
                    nameIsValid &&
                    emailIsValid &&
                    contactIsValid &&
                    officeContactIsValid &&
                    homeContactIsValid &&
                    addressIsValid &&
                    nameInput &&
                    contactInput &&
                    emailInput &&
                    addressInput
                  )
                }
              >
                {" "}
                {isUpdateState === false ? "Add" : "Update"}
              </button>
              <button
                type="button"
                className="btn btn-outline-secondary mx-2"
                onClick={clearForm}
              >
                Clear Form
              </button>
            </div>
          </form>
        </div>
      </div>
    </Fragment>
  );
}

export default ContactFormComponent;
