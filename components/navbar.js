import { useRouter } from "next/router";

/**
 * navbar component , to be used by every authenticated pages, responnsible for logout button activity
 */
function NavBar() {
  const router = useRouter();
  const logoutHandle = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("userId");

    router.push("/");
  };
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container ">
        <a className="navbar-brand" href="#">
          Leapfrog
        </a>
        <button className="btn btn-secondary" onClick={logoutHandle}>
          Logout
        </button>
      </div>
    </nav>
  );
}
export default NavBar;
