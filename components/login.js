import { Fragment } from "react/cjs/react.production.min";
import { useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";

/**
 * Login form, that allowes registered users to login, presented by home page
 */
function LoginComponent() {
  const router = useRouter();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailIsValid, setEmailValidation] = useState(true);

  // Input filed handler
  const emailHandler = (event) => {
    const email = event.target.value;
    setEmail(email);
    if (
      email &&
      email
        .toLowerCase()
        .match(
          /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        )
    ) {
      setEmailValidation(true);
    } else {
      setEmailValidation(false);
    }
  };

  // Input filed handler
  const passwordHandler = (event) => {
    setPassword(event.target.value);
  };

  // clears the form and sets to initial
  const clearForm = () => {
    setEmail("");
    setPassword("");
  };

  /**
   * submits form data to backend
   */
  const submitLoginForm = (event) => {
    event.preventDefault();
    const user = {
      userName: email,
      password: password,
    };
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(user),
    };

    fetch(
      // 'https://api.sumantgupta.com.np/auth/login',
      "http://localhost:3001/auth/login",
      requestOptions
    )
      .then((response) => response.json())
      .then((data) => {
        // if response is not okay
        if (data.statusCode === 500 || data.statusCode === 401) {
          alert("username or password wrong!!, try again");
        } else {
          // set the received token to local storage
          localStorage.setItem("token", data.token);
          localStorage.setItem("userId", data.loadedUserId);
          router.push("/contact");
        }
      });
    clearForm();
  };

  return (
    <Fragment>
      <div className="container mt-5">
        <div className="row">
          <div className="col-12 mt-5">
            <div className="card mx-auto login-card">
              <div className="card-body">
                <div className="text-center mb-4">
                  <img
                    src="https://angular.sumantgupta.com.np/assets/images/logo.png"
                    alt="logo"
                    height="80px"
                  />
                  <h4>Welcome</h4>
                  <small>Sign in by entering your informations below.</small>
                </div>

                <form onSubmit={submitLoginForm}>
                  <div className="mb-3">
                    <input
                      type="email"
                      className="form-control"
                      value={email}
                      onChange={emailHandler}
                      placeholder="name@example.com"
                    />
                    {emailIsValid === false ? (
                      <small className="inputError" style={{ color: "red" }}>
                        Please enter a valid email id.
                      </small>
                    ) : (
                      <></>
                    )}
                  </div>
                  <div className="mb-3">
                    <input
                      type="password"
                      className="form-control"
                      value={password}
                      onChange={passwordHandler}
                      placeholder="**********"
                      minLength="4"
                    />
                  </div>
                  <div className="mb-3">
                    <div className="d-grid gap-2">
                      <button
                        type="submit"
                        className="btn btn-success mx-2"
                        disabled={!(emailIsValid && password.length > 1)}
                      >
                        Login
                      </button>
                      <button
                        type="button"
                        className="btn btn-outline-secondary mx-2"
                        onClick={clearForm}
                      >
                        Cancel
                      </button>
                    </div>
                  </div>
                  <small>
                    <Link href="/auth/signup">
                      Don't have account? Sign up here.
                    </Link>
                  </small>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default LoginComponent;
