This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

#

Next js project, created by Sumant Kumar Gupta.
Requirement, (same as the requirement specified )

- user should be able to add contacts
- user should be able to register/login and then enter the system
- the system should be visible to only authenticated users, the login session should be managed by localstorage
- should use token based authentication system
- user should be able to filter the contacts by favorite and non-favourite, fav at the top.
- user should be able to add and update the contact from the single form
- more to it- multiple contacts, test cases, and contact image,

This is next js project,

- Idea: to learn something about Next js in React environment.
- Learning: Next js is self organised which keeps codes seggregared in pages and components, the routing is self tracked by Next through pages. It helps to build production ready apps. I thought its the one most people would be using but later found out that there are some limitations and so in most cases normal react project only is prepared in modules as components and pages and more organised by self.

# Overall Documentation of the project

- \_document.js - this file maintains the head,title,body (skeleton) structure of the code. It helps us to add header elements in the project document like cdn, scripts, styles etc that we may want to add in the project globally.
- pages : As Next detects pages as routes, here I have arranged accordingly. `auth` allows to bring pages inside /auth/ path where authentication related pages like login and signup are kept. Like wise, `contact` brings the main screen where contact CRUD operations happenns.
- components: It contains all the lower level components like contact form, listing contacts, signup form, login form etc.
- styles/globals.css : Next js allows component specific style encapsulation. Still since it was small project, all the styles needed are included in this style/globals.css file.
